/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Tasks.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Implements all the OS tasks. Maps software component runnables to tasks.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Os.h"
#include "IoHwAb.h"
#include "Btn.h"
#include "Led.h"
#include "Disp7Seg.h"
#include "PotMet.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Specifies the upper duty cycle limit to be used in the LEDs dimming. */
#define RTE_DIMMING_DUTY_UPPER_LIMIT           (0x2000UL)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

typedef enum
{
   RTE_STATE_MACHINE_IDLE,
   RTE_STATE_MACHINE_LEFT_FLASHING,
   RTE_STATE_MACHINE_RIGHT_FLASHING,
   RTE_STATE_MACHINE_HAZARD_FLASHING,

} Rte_StateMachineType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

///** \brief  Specifies the number of executed 100 ms tasks for time tracking purposes. */
//static uint8 Rte_uc_100MsTaskCounter = 0U;
//
///** \brief  Specifies the ID of LED that is influenced by the up-down dimming. */
//static uint8 Rte_uc_CurrentDimmingLed = 0U;
//
///** \brief  Specifies the current duty cycle to be applied on the current dimming influenced LED. */
//static uint32 Rte_ul_CurrentDuty = 0UL;
//
///** \brief  Specifies the current dimming state of the dimming influenced LED. */
//static Rte_DimmingType Rte_t_DimmingState = RTE_UPWARDS;
//
///** \brief  Specifies the current potentiometer reading. */
//static IoHwAb_AnalogLevelType Rte_t_PotentiometerValue;
uint8 counter_left = 0;
uint8 counter_right = 0;
uint8 counter_hazard = 0;
uint8 SignalCounter = 0;
uint8 numberOfFlashes = 4;
uint8 buttonFlag = 0;
uint8 testForHazars = 0;
uint8 Hazard_Block = 0;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

Btn_PressStateType Rte_gt_LeftLastState = BTN_NOT_PRESSED;
Btn_PressStateType Rte_gt_RightLastState = BTN_NOT_PRESSED;
Btn_PressStateType Rte_gt_HazardLastState = BTN_NOT_PRESSED;
Rte_StateMachineType Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_IDLE;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
void idleState();
void rightSignalign();
void leftSignalign();
void hazardSignalign();

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/
void idleState()
{

   Disp7Seg_SetState(DISP7SEG_INSTANCE_MYDISPLAY, 255, DISP7SEG_BOTH_ON);

   Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
   Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);

   Led_MainFunction();

   Disp7Seg_MainFunction();
}

void rightSignalign()
{

   Disp7Seg_SetState(DISP7SEG_INSTANCE_MYDISPLAY, numberOfFlashes, DISP7SEG_RIGHT_ON);
   counter_right++;
   if (counter_right < 100)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 9000);
      Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 9000);
      Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);
   }
   else if (counter_right < 200)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);
      Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);

   }

   else
   {
      numberOfFlashes--;
      counter_right = 0;
   }

   if (numberOfFlashes < 1)
   {
      idleState();
      numberOfFlashes = 4;
      Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_IDLE;
   }

//   if (counter >= 200)
//   {
//      counter = 0;
//   }

}

void leftSignalign()
{

   Disp7Seg_SetState(DISP7SEG_INSTANCE_MYDISPLAY, numberOfFlashes, DISP7SEG_LEFT_ON);
   counter_left++;
   if (counter_left < 100)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 9000);
      Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 9000);
      Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
   }
   else if (counter_left < 200)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);
      Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
   }
   else
   {
      numberOfFlashes--;
      counter_left = 0;
   }

   if (numberOfFlashes < 1)
   {
      idleState();
      numberOfFlashes = 4;
      Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_IDLE;
   }
}

void hazardSignalign()
{

   Disp7Seg_SetState(DISP7SEG_INSTANCE_MYDISPLAY, 255, DISP7SEG_BOTH_OFF);
   counter_hazard++;
   if (counter_hazard < 100)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 9000);
      Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 9000);
      Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 9000);
      Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 9000);
   }
   else if (counter_hazard < 200)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);
      Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
   }
   else
   {

      counter_hazard = 0;
   }

//      if (numberOfFlashes < 1)
//      {
//         idleState();
//         numberOfFlashes = 4;
//         Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_IDLE;
//      }

}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes RTE and all the SWCs.
 * \param      -
 * \return     -
 */
TASK(OS_INIT_TASK)
{
   Btn_Init();
   PotMet_Init();
   Led_Init();
   Disp7Seg_Init();
   idleState();
}

/**
 * \brief      Application specific 500 us periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_FAST_TASK)
{
}

/**
 * \brief      Application specific 5 ms periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_5MS_TASK)
{

   Btn_PressStateType t_LeftCurrentState;
   Btn_PressStateType t_RightCurrentState;
   Btn_PressStateType t_HazardCurrentState;

   //left

   t_LeftCurrentState = Btn_GetState(BTN_INSTANCE_LEFT);

   if ((Rte_gt_LeftLastState == BTN_NOT_PRESSED) && (t_LeftCurrentState == BTN_PRESSED))
   {
      if ((Rte_gt_CurrentStateMachine != RTE_STATE_MACHINE_HAZARD_FLASHING))
      {
         numberOfFlashes = 4;
         if (Rte_gt_CurrentStateMachine <= RTE_STATE_MACHINE_HAZARD_FLASHING)
         {
            Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_LEFT_FLASHING;
         }
      }

   }
   if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_IDLE)
   {
      idleState();
   }
   else if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_LEFT_FLASHING)
   {
      leftSignalign();
   }

//   if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_RIGHT_FLASHING)
//   {
//      rightSignalign();
//   }
//   else if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_RIGHT_FLASHING)
//   {
//      rightSignalign();
//   }
//   else if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_HAZARD_FLASHING)
//   {
//      hazardSignalign();
//   }

//right

   t_RightCurrentState = Btn_GetState(BTN_INSTANCE_RIGHT);

   if ((Rte_gt_RightLastState == BTN_NOT_PRESSED) && (t_RightCurrentState == BTN_PRESSED))
   {
      if ((Rte_gt_CurrentStateMachine != RTE_STATE_MACHINE_HAZARD_FLASHING))
      {
         numberOfFlashes = 4;
         if (Rte_gt_CurrentStateMachine <= RTE_STATE_MACHINE_HAZARD_FLASHING)
         {
            Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_RIGHT_FLASHING;

         }
      }

   }

   if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_IDLE)
   {
      idleState();
   }
   else if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_RIGHT_FLASHING)
   {
      rightSignalign();
   }

//   if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_LEFT_FLASHING)
//   {
//      leftSignalign();
//   }
//   else if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_LEFT_FLASHING)
//   {
//      rightSignalign();
//   }
//   else if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_HAZARD_FLASHING)
//   {
//      hazardSignalign();
//
//   }

//hazard

   t_HazardCurrentState = Btn_GetState(BTN_INSTANCE_HAZARD);

   if ((Rte_gt_HazardLastState == BTN_NOT_PRESSED) && (t_HazardCurrentState == BTN_PRESSED))
   {
      if (Rte_gt_CurrentStateMachine <= RTE_STATE_MACHINE_RIGHT_FLASHING)
      {
         Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_HAZARD_FLASHING;
      }
      else
      {
         Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_IDLE;
         idleState();
      }

   }

   if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_HAZARD_FLASHING)
   {
      hazardSignalign();
   }

   //hazard off
//   if (t_CurrentState == BTN_NOT_PRESSED)
//      {
//         testForHazars = 1;
//      }
//
//   t_CurrentState = Btn_GetState(BTN_INSTANCE_HAZARD);
//
//
//   if ((Hazard_Block==1)&&(testForHazars == 1))
//   {
//      if (t_CurrentState == BTN_PRESSED)
//      {
//         Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_IDLE;
//         testForHazars = 0;
//      }
//
//      if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_IDLE)
//      {
//         idleState();
//      }
//   }

   Led_MainFunction();

   Disp7Seg_MainFunction();
   Btn_MainFunction();
   PotMet_MainFunction();

   Rte_gt_LeftLastState = t_LeftCurrentState;
   Rte_gt_RightLastState = t_RightCurrentState;
   Rte_gt_HazardLastState = t_HazardCurrentState;

}

/**
 * \brief      Application specific 10 ms periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_10MS_TASK)
{

}

/**
 * \brief      Application specific 20 ms periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_20MS_TASK)
{
//   Rte_t_PotentiometerValue = IoHwAb_AnalogGetChannel(IOHWAB_ANALOG_POTENTIOMETER);
//
// switch (Rte_t_DimmingState)
//   {
//      case (RTE_UPWARDS):
//      {
//         if (Rte_ul_CurrentDuty >= (RTE_DIMMING_DUTY_UPPER_LIMIT - (Rte_t_PotentiometerValue + 100UL)))
//         {
//            Rte_ul_CurrentDuty = RTE_DIMMING_DUTY_UPPER_LIMIT;
//            Rte_t_DimmingState = RTE_DOWNWARDS;
//         }
//         else
//         {
//            Rte_ul_CurrentDuty += (Rte_t_PotentiometerValue + 100UL);
//         }
//         break;
//      }
//
//      case (RTE_DOWNWARDS):
//      {
//         if (Rte_ul_CurrentDuty <= (Rte_t_PotentiometerValue + 100UL))
//         {
//            Rte_ul_CurrentDuty = 0UL;
//            Rte_uc_CurrentDimmingLed = (Rte_uc_CurrentDimmingLed + 1U) % 4U;
//            Rte_t_DimmingState = RTE_UPWARDS;
//         }
//         else
//         {
//            Rte_ul_CurrentDuty -= (Rte_t_PotentiometerValue + 100UL);
//         }
//         break;
//      }
//
//      default:
//      {
//         break;
//      }
//   }
//
//   switch (Rte_uc_CurrentDimmingLed)
//   {
//      case (0U):
//      {
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_FRONT_LEFT, Rte_ul_CurrentDuty);
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_FRONT_RIGHT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_BACK_LEFT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_BACK_RIGHT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         break;
//      }
//      case (1U):
//      {
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_FRONT_LEFT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_FRONT_RIGHT, Rte_ul_CurrentDuty);
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_BACK_LEFT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_BACK_RIGHT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         break;
//      }
//      case (2U):
//      {
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_FRONT_LEFT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_FRONT_RIGHT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_BACK_LEFT, Rte_ul_CurrentDuty);
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_BACK_RIGHT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         break;
//      }
//      case (3U):
//      {
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_FRONT_LEFT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_FRONT_RIGHT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_BACK_LEFT, (RTE_DIMMING_DUTY_UPPER_LIMIT / 100UL));
//         IoHwAb_PwmSetChannel(IOHWAB_PWM_LED_BACK_RIGHT, Rte_ul_CurrentDuty);
//         break;
//      }
//   }
}

/**
 * \brief      Application specific 100 ms periodicity task.
 * \param     -
 * \return    -
 */
TASK(OS_100MS_TASK)
{

//   if (numberOfFlashes > 0)
//      {
//         Disp7Seg_SetState(DISP7SEG_INSTANCE_MYDISPLAY, numberOfFlashes, DISP7SEG_BOTH_ON);
//         numberOfFlashes--;
//      }

//   else if (STD_HIGH == IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_CHANNEL_BTN_RIGHT))
//   {
//      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0x80FFU);
//      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0x80FFU);
//   }
//   else if (STD_HIGH == IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_CHANNEL_BTN_HAZARD))
//   {
//      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0x80AAU);
//      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0x80AAU);
//   }
//   else
//   {
//      if (Rte_uc_100MsTaskCounter >= 10U)
//      {
//         Rte_uc_100MsTaskCounter = 0U;
//
//         IoHwAb_DigitalFlipChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A);
//         IoHwAb_DigitalFlipChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B);
//         IoHwAb_DigitalFlipChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C);
//         IoHwAb_DigitalFlipChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D);
//         IoHwAb_DigitalFlipChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E);
//         IoHwAb_DigitalFlipChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F);
//         IoHwAb_DigitalFlipChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G);
//         IoHwAb_DigitalFlipChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1);
//
//
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_LOW);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_LOW);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_LOW);
//
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
//
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_LOW);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_LOW);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_LOW);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_LOW);
//      }
//
//      if (Rte_uc_100MsTaskCounter == 5U)
//      {
//
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_HIGH);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_HIGH);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_HIGH);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
//
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_LOW);
//
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);
//         IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_HIGH);
//      }
//      Rte_uc_100MsTaskCounter++;
//   }
}

/**
 * \brief      Application background task.
 * \param      -
 * \return     -
 */
TASK(OS_BACKGROUND_TASK)
{
}
