/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Disp7Seg.c
 *    \author     Ciumarnean Raul
 *    \brief      This file contains  functionality for 7segment display
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Disp7Seg.h"
#include "IoHwAb.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

static Disp7_Specif Disp7_array[2];
uint8 left_digit = 255;
uint8 right_digit = 255;
Disp7Seg_DotsType dots_state_display;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/
void leftNumber(uint8 left_digit)
{
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_LOW);

   switch (left_digit)
   {
      case 0:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);

         break;

      case 1:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);

         break;

      case 2:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_HIGH);

         break;

      case 3:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);

         break;

      case 4:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);

         break;

      case 5:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);

         break;

      case 6:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_HIGH);

         break;

      case 7:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);

      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);

         break;

      case 8:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
         break;

      case 9:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
         break;
      case 255:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);

         break;
   }

}

void rightNumber(uint8 right_digit)
{

   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_LOW);

   switch (right_digit)
   {
      case 0:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);

         break;

      case 1:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);

         break;

      case 2:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_HIGH);

         break;

      case 3:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);

         break;

      case 4:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);

         break;

      case 5:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);

         break;

      case 6:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_HIGH);

         break;

      case 7:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);

      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);

         break;

      case 8:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
         break;

      case 9:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
         break;

      case 255:
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);
         break;

   }

}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
void Disp7Seg_Init()
{

   Disp7_array[0].disp_pin = IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1;
   Disp7_array[0].decimal_value = 0;
   IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0x0000U);

   Disp7_array[1].disp_pin = IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2;
   Disp7_array[1].decimal_value = 0;
   IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0x0000U);

}
void Disp7Seg_MainFunction()
{
   leftNumber(left_digit);
   rightNumber(right_digit);
   if (dots_state_display == DISP7SEG_BOTH_OFF)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_HIGH);
   }
   else if (dots_state_display == DISP7SEG_BOTH_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_LOW);
   }
   else if (dots_state_display == DISP7SEG_LEFT_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_HIGH);
   }
   else if (dots_state_display == DISP7SEG_RIGHT_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_LOW);
   }

}
void Disp7Seg_SetState(uint8 uc_Id, uint8 uc_DecValue, Disp7Seg_DotsType t_Dots)
{
   uc_Id++; // just for warning
   if (uc_DecValue < 100)
   {
      left_digit = uc_DecValue / 10;
      right_digit = uc_DecValue % 10;
      dots_state_display = t_Dots;
   }
   else if (uc_DecValue == 255)
   {
      left_digit = 255;
      right_digit = 255;
      dots_state_display = t_Dots;

   }

   else
   {
   }
}

