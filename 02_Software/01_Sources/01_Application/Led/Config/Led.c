/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       PotMet.c
 *    \author     Ciumarnean Raul
 *    \brief
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Led.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
static Led_Specif Led_array[4];
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

static uint16 Interpolare_Led(uint16 input)
{
   uint16 output;

   uint16 input_min = 0;
   uint16 input_max = 10000;
   uint16 output_max = 0x8000;
   uint16 output_min = 0x0000;

   output = ((uint32) (output_max * (input - input_min)) + output_min * (input - input_max)) / (input_max - input_min);

   return output;
}
/**
 *-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

void Led_Init(void)
{





   Led_array[LED_INSTANCE_LED_FRONT_RIGHT].led_pin = IOHWAB_PWM_LED_FRONT_RIGHT;
   Led_array[LED_INSTANCE_LED_FRONT_RIGHT].duty = 0;
   IoHwAb_PwmSetChannel(Led_array[0].led_pin, Led_array[0].duty);

   Led_array[LED_INSTANCE_LED_BACK_RIGHT].led_pin = IOHWAB_PWM_LED_BACK_RIGHT;
   Led_array[LED_INSTANCE_LED_BACK_RIGHT].duty = 0;
   IoHwAb_PwmSetChannel(Led_array[1].led_pin, Led_array[2].duty);

   Led_array[LED_INSTANCE_LED_FRONT_LEFT].led_pin = IOHWAB_PWM_LED_FRONT_LEFT;
   Led_array[LED_INSTANCE_LED_FRONT_LEFT].duty = 0;
   IoHwAb_PwmSetChannel(Led_array[2].led_pin, Led_array[2].duty);

   Led_array[LED_INSTANCE_LED_BACK_LEFT].led_pin = IOHWAB_PWM_LED_BACK_LEFT;
   Led_array[LED_INSTANCE_LED_BACK_LEFT].duty = 0;
   IoHwAb_PwmSetChannel(Led_array[3].led_pin, Led_array[3].duty);

}

void Led_MainFunction(void)
{
   for (int i = 0; i < 4; i++)
      IoHwAb_PwmSetChannel(Led_array[i].led_pin, Led_array[i].duty);

}

void Led_SetState(uint8 uc_Id, Led_DutyType t_Duty)
{
   static uint16 result_interpolate = 0;

   if (t_Duty <= 10000)
   {
      result_interpolate = Interpolare_Led(t_Duty);
      Led_array[uc_Id].duty = result_interpolate;

   }

}
