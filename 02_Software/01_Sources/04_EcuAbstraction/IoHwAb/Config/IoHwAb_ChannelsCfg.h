/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       IoHwAb_ChannelsCfg.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Defines the sensor and actuator channels on the ECU abstraction level for accessing all the ECU
 *                signals.
 *
 *    From the MCAL drivers' perspective the channels do not contain the information about the context they're used in.
 *    In this layer the MCAL channels are mapped to ECU signal names that describe the signals origin or destination
 *    that are correlated with a sensor or an actuator.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef IOHWAB_CHANNELSCFG_H
#define IOHWAB_CHANNELSCFG_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "IoHwAb_ApiCfg.h"

#if (STD_ON == IOHWAB_API)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/* Redefine all the MCAL driver channels by providing the information about the used peripherals. */

/* -------------------------- Analog ---------------------------*/

#if (STD_ON == IOHWAB_ANALOG_API)

#define IOHWAB_ANALOG_POTENTIOMETER                      (ADC_GROUP_C_5)

#endif /* (STD_ON == IOHWAB_ANALOG_API) */

/* -------------------------- Digital --------------------------*/

#if (STD_ON == IOHWAB_DIGITAL_API)

#define IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT          (DIO_PORT_A)
#define IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT   		(DIO_PORT_B)

#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A           (DIO_CHANNEL_A_0)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B           (DIO_CHANNEL_A_1)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C           (DIO_CHANNEL_A_2)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D           (DIO_CHANNEL_A_3)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E           (DIO_CHANNEL_A_4)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F           (DIO_CHANNEL_A_5)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G           (DIO_CHANNEL_A_6)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1         (DIO_CHANNEL_A_7)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1        (DIO_CHANNEL_A_15)

#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A          (DIO_CHANNEL_B_0)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B          (DIO_CHANNEL_B_1)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C          (DIO_CHANNEL_B_2)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D          (DIO_CHANNEL_B_3)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E          (DIO_CHANNEL_B_4)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F          (DIO_CHANNEL_B_5)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G          (DIO_CHANNEL_B_6)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2        (DIO_CHANNEL_B_7)
#define IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2       (DIO_CHANNEL_B_15)

#define IOHWAB_DIGITAL_CHANNEL_BTN_LEFT                  (DIO_CHANNEL_D_0)
#define IOHWAB_DIGITAL_CHANNEL_BTN_RIGHT                 (DIO_CHANNEL_D_1)
#define IOHWAB_DIGITAL_CHANNEL_BTN_HAZARD                (DIO_CHANNEL_D_2)

#endif /* (STD_ON == IOHWAB_DIGITAL_API) */

/* ---------------------------- PWM ----------------------------*/

#if (STD_ON == IOHWAB_PWM_API)

#define IOHWAB_PWM_LED_FRONT_LEFT                        (PWM_CHANNEL_C_6)
#define IOHWAB_PWM_LED_FRONT_RIGHT                       (PWM_CHANNEL_C_7)
#define IOHWAB_PWM_LED_BACK_LEFT                         (PWM_CHANNEL_C_8)
#define IOHWAB_PWM_LED_BACK_RIGHT                        (PWM_CHANNEL_C_9)

#endif /* (STD_ON == IOHWAB_PWM_API) */

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#endif /* (STD_ON == IOHWAB_API) */

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* IOHWAB_CHANNELSCFG_H */
