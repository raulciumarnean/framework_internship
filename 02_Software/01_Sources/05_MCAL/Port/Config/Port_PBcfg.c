/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Port_PBcfg.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Sets up the AUTOSAR PORT driver configuration using the RegInit data types for masked 32 bits
 *                registers. Sets up mode, direction and speed of all used pins.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Port.h"
#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the total number of registers to be loaded in the initialization function. */
#define PORT_NUMBER_OF_INIT_REGISTERS      (10U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Stores the configuration of all the registers to be set in the PORT initialization function. */
static const RegInit_Masked32BitsSingleType Port_kat_InitLoadRegisters[PORT_NUMBER_OF_INIT_REGISTERS] =
{
   /* Set port A pins 0 .. 7 and 15 to very high speed. */
   {
      &GPIOA->OSPEEDR,

      (uint32) ~(
      GPIO_OSPEEDER_OSPEEDR15 |
      GPIO_OSPEEDER_OSPEEDR7 |
      GPIO_OSPEEDER_OSPEEDR6 |
      GPIO_OSPEEDER_OSPEEDR5 |
      GPIO_OSPEEDER_OSPEEDR4 |
      GPIO_OSPEEDER_OSPEEDR3 |
      GPIO_OSPEEDER_OSPEEDR2 |
      GPIO_OSPEEDER_OSPEEDR1 |
      GPIO_OSPEEDER_OSPEEDR0),

      (uint32) (
      GPIO_OSPEEDER_OSPEEDR15 |
      GPIO_OSPEEDER_OSPEEDR7 |
      GPIO_OSPEEDER_OSPEEDR6 |
      GPIO_OSPEEDER_OSPEEDR5 |
      GPIO_OSPEEDER_OSPEEDR4 |
      GPIO_OSPEEDER_OSPEEDR3 |
      GPIO_OSPEEDER_OSPEEDR2 |
      GPIO_OSPEEDER_OSPEEDR1 |
      GPIO_OSPEEDER_OSPEEDR0),
   },

   /* Set port A pins 0 .. 7 and 15 to general purpose output. */
   {
      &GPIOA->MODER,

      (uint32) ~(
      GPIO_MODER_MODER15 |
      GPIO_MODER_MODER7 |
      GPIO_MODER_MODER6 |
      GPIO_MODER_MODER5 |
      GPIO_MODER_MODER4 |
      GPIO_MODER_MODER3 |
      GPIO_MODER_MODER2 |
      GPIO_MODER_MODER1 |
      GPIO_MODER_MODER0),

      (uint32) (
      GPIO_MODER_MODER15_0 |
      GPIO_MODER_MODER7_0 |
      GPIO_MODER_MODER6_0 |
      GPIO_MODER_MODER5_0 |
      GPIO_MODER_MODER4_0 |
      GPIO_MODER_MODER3_0 |
      GPIO_MODER_MODER2_0 |
      GPIO_MODER_MODER1_0 |
      GPIO_MODER_MODER0_0),
   },

   /* Set port B pins 0 .. 7 and 15 to very high speed. */
   {
      &GPIOB->OSPEEDR,

      (uint32) ~(
      GPIO_OSPEEDER_OSPEEDR15 |
      GPIO_OSPEEDER_OSPEEDR7 |
      GPIO_OSPEEDER_OSPEEDR6 |
      GPIO_OSPEEDER_OSPEEDR5 |
      GPIO_OSPEEDER_OSPEEDR4 |
      GPIO_OSPEEDER_OSPEEDR3 |
      GPIO_OSPEEDER_OSPEEDR2 |
      GPIO_OSPEEDER_OSPEEDR1 |
      GPIO_OSPEEDER_OSPEEDR0),

      (uint32) (
      GPIO_OSPEEDER_OSPEEDR15 |
      GPIO_OSPEEDER_OSPEEDR7 |
      GPIO_OSPEEDER_OSPEEDR6 |
      GPIO_OSPEEDER_OSPEEDR5 |
      GPIO_OSPEEDER_OSPEEDR4 |
      GPIO_OSPEEDER_OSPEEDR3 |
      GPIO_OSPEEDER_OSPEEDR2 |
      GPIO_OSPEEDER_OSPEEDR1 |
      GPIO_OSPEEDER_OSPEEDR0),
   },

   /* Set port B pins 0 .. 7 and 15 to general purpose output. */
   {
      &GPIOB->MODER,

      (uint32) ~(
      GPIO_MODER_MODER15 |
      GPIO_MODER_MODER7 |
      GPIO_MODER_MODER6 |
      GPIO_MODER_MODER5 |
      GPIO_MODER_MODER4 |
      GPIO_MODER_MODER3 |
      GPIO_MODER_MODER2 |
      GPIO_MODER_MODER1 |
      GPIO_MODER_MODER0),

      (uint32) (
      GPIO_MODER_MODER15_0 |
      GPIO_MODER_MODER7_0 |
      GPIO_MODER_MODER6_0 |
      GPIO_MODER_MODER5_0 |
      GPIO_MODER_MODER4_0 |
      GPIO_MODER_MODER3_0 |
      GPIO_MODER_MODER2_0 |
      GPIO_MODER_MODER1_0 |
      GPIO_MODER_MODER0_0),
   },

   /* Set port C pins 5 .. 9 to very high speed. */
   {
      &GPIOC->OSPEEDR,

      (uint32) ~(
      GPIO_OSPEEDER_OSPEEDR9 |
      GPIO_OSPEEDER_OSPEEDR8 |
      GPIO_OSPEEDER_OSPEEDR7 |
      GPIO_OSPEEDER_OSPEEDR6 |
      GPIO_OSPEEDER_OSPEEDR5),

      (uint32) (
      GPIO_OSPEEDER_OSPEEDR9 |
      GPIO_OSPEEDER_OSPEEDR8 |
      GPIO_OSPEEDER_OSPEEDR7 |
      GPIO_OSPEEDER_OSPEEDR6 |
      GPIO_OSPEEDER_OSPEEDR5),
   },

   /* Set port C pin 5 to analog and pins 6 .. 9 to alternate function. */
   {
      &GPIOC->MODER,

      (uint32) ~(
      GPIO_MODER_MODER9 |
      GPIO_MODER_MODER8 |
      GPIO_MODER_MODER7 |
      GPIO_MODER_MODER6 |
      GPIO_MODER_MODER5),

      (uint32) (
      GPIO_MODER_MODER9_1 |
      GPIO_MODER_MODER8_1 |
      GPIO_MODER_MODER7_1 |
      GPIO_MODER_MODER6_1 |
      GPIO_MODER_MODER5),
   },

   /* Set port C pins 6 and 7 to alternate function AF2. */
   {
      &GPIOC->AFRL,

      (uint32) ~(
      GPIO_AFRL_AFRL7 |
      GPIO_AFRL_AFRL6),

      (uint32) (
      GPIO_AFRL_AFRL7_1 |
      GPIO_AFRL_AFRL6_1)
   },

   /* Set port C pins 8 and 9 to alternate function AF2. */
   {
      &GPIOC->AFRH,

      (uint32) ~(
      GPIO_AFRH_AFRH9 |
      GPIO_AFRH_AFRH8),

      (uint32) (
      GPIO_AFRH_AFRH9_1 |
      GPIO_AFRH_AFRH8_1)
   },

   /* Set port D pins 0, 1 and 2 to very high speed. */
   {
      &GPIOD->OSPEEDR,

      (uint32) ~(
      GPIO_OSPEEDER_OSPEEDR2 |
      GPIO_OSPEEDER_OSPEEDR1 |
      GPIO_OSPEEDER_OSPEEDR0),

      (uint32) (
      GPIO_OSPEEDER_OSPEEDR2 |
      GPIO_OSPEEDER_OSPEEDR1 |
      GPIO_OSPEEDER_OSPEEDR0),
   },

   /* Set port D pins 0, 1 and 2 to input. */
   {
      &GPIOD->MODER,

      (uint32) ~(
      GPIO_MODER_MODER2 |
      GPIO_MODER_MODER1 |
      GPIO_MODER_MODER0),

      0UL,
   },
};

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  References the configuration of all the registers to be set in the PORT initialization function and the
 *          number of registers to be initialized. */
const Port_ConfigType Port_gkt_Config =
{ Port_kat_InitLoadRegisters, PORT_NUMBER_OF_INIT_REGISTERS };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
