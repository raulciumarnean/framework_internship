/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Dio_Lcfg.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Sets up the AUTOSAR DIO link-time configurations for all individual channels, channel groups and
 *                ports.
 *
 *    All the configurations are stored in instance specific tables, each index representing the configuration of an
 *    instance. The configured port register addresses are stored in only one table (for the port instances) and are
 *    referenced through the port IDs from the other tables.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Dio.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the port register addresses that are used for all instances.
 *
 * These addresses are used for all full port, individual channel and channel group read / write operations. Each index
 * is correlated with one port ID. */
const Dio_PortCfgType Dio_gkat_PortAdresses[DIO_NUMBER_OF_PORTS] =
{
   { GPIOA },                          // DIO_PORT_A
   { GPIOB },                          // DIO_PORT_B
   { GPIOD },                          // DIO_PORT_B
};

#if (STD_ON == DIO_CHANNELS_API)
/** \brief  Defines the configuration of all the individual channels.
 *
 * Each individual channel is defined by a port mask and a port ID. The port ID is used for getting the actual register
 * address from the table containing the port register address. Each index is correlated with one DIO channel ID. */
const Dio_ChannelCfgType Dio_gkat_Channels[DIO_NUMBER_OF_CHANNELS] =
{
   { GPIO_IDR_IDR_0, DIO_PORT_A },     // DIO_CHANNEL_A_0
   { GPIO_IDR_IDR_1, DIO_PORT_A },     // DIO_CHANNEL_A_1
   { GPIO_IDR_IDR_2, DIO_PORT_A },     // DIO_CHANNEL_A_2
   { GPIO_IDR_IDR_3, DIO_PORT_A },     // DIO_CHANNEL_A_3
   { GPIO_IDR_IDR_4, DIO_PORT_A },     // DIO_CHANNEL_A_4
   { GPIO_IDR_IDR_5, DIO_PORT_A },     // DIO_CHANNEL_A_5
   { GPIO_IDR_IDR_6, DIO_PORT_A },     // DIO_CHANNEL_A_6
   { GPIO_IDR_IDR_7, DIO_PORT_A },     // DIO_CHANNEL_A_7
   { GPIO_IDR_IDR_15, DIO_PORT_A },    // DIO_CHANNEL_A_15
   { GPIO_IDR_IDR_0, DIO_PORT_B },     // DIO_CHANNEL_B_0
   { GPIO_IDR_IDR_1, DIO_PORT_B },     // DIO_CHANNEL_B_1
   { GPIO_IDR_IDR_2, DIO_PORT_B },     // DIO_CHANNEL_B_2
   { GPIO_IDR_IDR_3, DIO_PORT_B },     // DIO_CHANNEL_B_3
   { GPIO_IDR_IDR_4, DIO_PORT_B },     // DIO_CHANNEL_B_4
   { GPIO_IDR_IDR_5, DIO_PORT_B },     // DIO_CHANNEL_B_5
   { GPIO_IDR_IDR_6, DIO_PORT_B },     // DIO_CHANNEL_B_6
   { GPIO_IDR_IDR_7, DIO_PORT_B },     // DIO_CHANNEL_B_7
   { GPIO_IDR_IDR_15, DIO_PORT_B },    // DIO_CHANNEL1_15
   { GPIO_IDR_IDR_0, DIO_PORT_D },     // DIO_CHANNEL_C_0
   { GPIO_IDR_IDR_1, DIO_PORT_D },     // DIO_CHANNEL_C_1
   { GPIO_IDR_IDR_2, DIO_PORT_D },     // DIO_CHANNEL_C_2
};
#endif

#if (STD_ON == DIO_CHANNEL_GROUPS_API)
/** \brief  Defines the configuration of all the channel groups.
 *
 * Each channel group is defined by a port mask, an offset and a port ID. The port ID is used for getting the actual
 * register address from the table containing the port register address, while the mask and offset are used for read or
 * write operations on the desired channels. Each index is exported as pointer through the channel group IDs.*/
const Dio_ChannelGroupType Dio_gkat_ChannelGroups[DIO_NUMBER_OF_CHANNEL_GROUPS] =
{
   { 0xC000U, 14U, DIO_PORT_D },       // DIO_CHANNEL_GROUP_D_14_15
   { 0x6000U, 13U, DIO_PORT_D },       // DIO_CHANNEL_GROUP_D_13_14
   { 0x0006U, 1U, DIO_PORT_A },        // DIO_CHANNEL_GROUP_A_1_2
};
#endif

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
