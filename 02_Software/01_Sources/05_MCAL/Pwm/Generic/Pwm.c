/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pwm.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Implements the AUTOSAR PWM initialization function and the interfaces for controlling all the
 *                configured PWM channels.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Pwm.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Contains a pointer to the selected PWM driver post-build channels configuration structure. */
static Pwm_ChannelCtrlType * Pwm_pt_ChannelsCtrl;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Sets up the timers and output compare registers for PWM mode and loads the channels control
 *             configuration for runtime use.
 * \param      ConfigPtr : Pointer to the post-build configuration data to be loaded.
 * \return     -
 */
void Pwm_Init(const Pwm_ConfigType* ConfigPtr)
{
   /* Initialize all the timer and output compare registers for PWM mode. */
   RegInit_gv_Masked32Bits(ConfigPtr->pt_InitConfig);

   /* Load the channels control configuration. */
   Pwm_pt_ChannelsCtrl = (Pwm_ChannelCtrlType *) ConfigPtr->pt_ChannelsCtrl;
}

/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == PWM_SET_DUTY_CYCLE_API)
/**
 * \brief      Sets the specified duty cycle to the specified PWM channel.
 * \param      ChannelNumber : ID of a specific PWM channel.
 * \param      DutyCycle : Duty cycle to be applied to the specified channel (0..0x8000).
 * \return     -
 */
void Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber, uint16 DutyCycle)
{
   Pwm_ChannelCtrlType t_ChannelCtrl;

   /* Read the channel configuration to avoid multiple indexing. */
   t_ChannelCtrl = Pwm_pt_ChannelsCtrl[ChannelNumber];

   /* Set the specified duty cycle to the specified PWM channel. A conversion from AUTOSAR duty cycle format
    * (0..0x8000) to register format (depending on the ARR period register) is needed.
    * (0 .. 0x8000 -> 0 .. (ARR + 1)). The used formula is an optimized form of:
    *    CCRx = (((ARR + 1) * DutyCycle) / 0x8000).
    *
    * The offset of 1 is needed for being able to reach a duty cycle of 100%. The PWM output signal is set to the
    * configured active value as long as TIMx_CNT < TIMx_CCRx, else it becomes the inactive value. If the compare value
    * in TIMx_CCRx is greater than TIMx_ARR then the PWM output signal is held at the active value. If TIMx_CCRx is 0
    * then the PWM output signal is held at the inactive value. */
   *t_ChannelCtrl.pul_DutyCycleReg = (uint32) (((*t_ChannelCtrl.pul_PeriodReg + 1UL) * (uint32) DutyCycle) >> 15UL);
}
#endif

/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == PWM_SET_PERIOD_AND_DUTY_API)
/**
 * \brief      Sets the specified duty cycle and period to the specified PWM channel.
 * \param      ChannelNumber : ID of a specific PWM channel.
 * \param      Period : Period to be applied to the specified channel.
 * \param      DutyCycle : Duty cycle to be applied to the specified channel (0..0x8000).
 * \return     -
 */
void Pwm_SetPeriodAndDuty(Pwm_ChannelType ChannelNumber, Pwm_PeriodType Period, uint16 DutyCycle)
{
   Pwm_ChannelCtrlType t_ChannelCtrl;

   /* Read the channel configuration to avoid multiple indexing. */
   t_ChannelCtrl = Pwm_pt_ChannelsCtrl[ChannelNumber];

   /* Set the specified period in ticks to the specified PWM channel. */
   *t_ChannelCtrl.pul_PeriodReg = (uint32) Period;

   /* Set the specified duty cycle to the specified PWM channel. A conversion from AUTOSAR duty cycle format
    * (0..0x8000) to register format (depending on the ARR period register) is needed.
    * (0 .. 0x8000 -> 0 .. (ARR + 1)). The used formula is an optimized form of:
    *    CCRx = (((ARR + 1) * DutyCycle) / 0x8000).
    *
    * The offset of 1 is needed for being able to reach a duty cycle of 100%. The PWM output signal is set to the
    * configured active value as long as TIMx_CNT < TIMx_CCRx, else it becomes the inactive value. If the compare value
    * in TIMx_CCRx is greater than TIMx_ARR then the PWM output signal is held at the active value. If TIMx_CCRx is 0
    * then the PWM output signal is held at the inactive value. */
   *t_ChannelCtrl.pul_DutyCycleReg = (uint32) ((((uint32) Period + 1UL) * (uint32) DutyCycle) >> 15UL);
}
#endif

/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == PWM_SET_OUTPUT_TO_IDLE_API)
/**
 * \brief      Sets the specified PWM channel to its configured idle state.
 * \param      ChannelNumber : ID of a specific PWM channel.
 * \return     -
 *
 * As a rule, the PWM channels are always configured as up-counting, edge-aligned, PWM mode 1. The output idle state
 * depends on the configured output compare active value (CCxP bit from the CCER register) which matches the configured
 * AUTOSAR defined polarity (PWM_HIGH or PWM_LOW). For setting the output idle state it is sufficient to set the
 * PWM channel's capture / compare register (CCRx) to 0 or ARR + 1 with respect to the configured polarity.
 */
void Pwm_SetOutputToIdle(Pwm_ChannelType ChannelNumber)
{
   Pwm_ChannelCtrlType t_ChannelCtrl;

   /* Read the channel configuration to avoid multiple indexing. */
   t_ChannelCtrl = Pwm_pt_ChannelsCtrl[ChannelNumber];

   /* Set the CCRx register depending on the configured polarity and output idle state. */
   if (PWM_LOW == t_ChannelCtrl.t_Polarity)
   {
      if (PWM_LOW == t_ChannelCtrl.t_OutputIdleState)
      {
         /* In this case TIMx_CNT < TIMx_CCRx (always), so the output will be held at PWM_LOW. */
         *t_ChannelCtrl.pul_DutyCycleReg = *t_ChannelCtrl.pul_PeriodReg + 1UL;
      }
      else /* (PWM_HIGH == t_ChannelCtrl.t_OutputIdleState) */
      {
         /* In this case TIMx_CNT >= TIMx_CCRx (always), so the output will be held at PWM_HIGH. */
         *t_ChannelCtrl.pul_DutyCycleReg = 0UL;
      }
   }
   else /* (PWM_HIGH == t_ChannelCtrl.t_Polarity) */
   {
      if (PWM_LOW == t_ChannelCtrl.t_OutputIdleState)
      {
         /* In this case TIMx_CNT >= TIMx_CCRx (always), so the output will be held at PWM_LOW. */
         *t_ChannelCtrl.pul_DutyCycleReg = 0UL;
      }
      else /* (PWM_HIGH == t_ChannelCtrl.t_OutputIdleState) */
      {
         /* In this case TIMx_CNT < TIMx_CCRx (always), so the output will be held at PWM_HIGH. */
         *t_ChannelCtrl.pul_DutyCycleReg = *t_ChannelCtrl.pul_PeriodReg + 1UL;
      }
   }
}
#endif

/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == PWM_GET_OUTPUT_STATE_API)
/**
 * \brief      Reads the output state from the specified PWM channel.
 * \param      ChannelNumber : ID of a specific PWM channel.
 * \return     Current output state (PWM_LOW or PWM_HIGH) on the specified PWM channel.
 *
 * The implementation of this function is not according to "Specification of PWM Driver AUTOSAR CP Release 4.3.1",
 * specification [SWS_Pwm_00022]. The reason is that the PWM unit does not allow a direct way of reading the internal
 * state of the PWM output signal, the only possibility being the usage of interrupts on UIF and CCxIF flags. Using
 * such complex mechanisms for such a simple function is not acceptable, therefore, the value of the PWM output signal
 * is sampled from the input data register the PWM channel is routed to.
 */
Pwm_OutputStateType Pwm_GetOutputState(Pwm_ChannelType ChannelNumber)
{
   Pwm_OutputStateType t_ReturnValue;
   Pwm_ChannelCtrlType t_ChannelCtrl;

   /* Read the channel configuration to avoid multiple indexing. */
   t_ChannelCtrl = Pwm_pt_ChannelsCtrl[ChannelNumber];

   /* Read the input data register and apply the corresponding mask for identifying if the PWM output is in PWM_HIGH
    * or PWM_LOW state. */
   if (((uint16) *t_ChannelCtrl.pul_InputDataReg & t_ChannelCtrl.us_InputDataMask) != 0U)
   {
      t_ReturnValue = PWM_HIGH;
   }
   else
   {
      t_ReturnValue = PWM_LOW;
   }

   return t_ReturnValue;
}
#endif
